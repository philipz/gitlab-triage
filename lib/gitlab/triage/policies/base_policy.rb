# frozen_string_literal: true

module Gitlab
  module Triage
    module Policies
      class BasePolicy
        attr_reader :type, :policy_spec, :resources, :network

        def initialize(type, policy_spec, resources, network)
          @type = type
          @policy_spec = policy_spec
          # In some filters/actions we want to know which resource type it is
          @resources = attach_resource_type(resources, type)
          @network = network
        end

        def name
          @name ||= (policy_spec[:name] || "#{type}-#{object_id}")
        end

        def actions
          @actions ||= policy_spec.fetch(:actions) { {} }
        end

        def summarize?
          actions.key?(:summarize)
        end

        def comment?
          # The actual keys are strings
          (actions.keys.map(&:to_sym) - [:summarize]).any?
        end

        def build_issue
          raise NotImplementedError
        end

        private

        # We don't have to do this once the response will contain the type
        # of the resource. For now let's just attach it.
        def attach_resource_type(resources, type)
          resources.map { |resource| resource.reverse_merge(type: type) }
        end
      end
    end
  end
end
