module Gitlab
  module Triage
    Options = Struct.new(
      :dry_run,
      :policies_file,
      :source,
      :source_id,
      :token,
      :debug,
      :host_url,
      :api_version
    ) do
      def initialize(*args)
        super

        # Defaults
        self.host_url ||= 'https://gitlab.com'
        self.api_version ||= 'v4'
        self.source ||= 'projects'
      end
    end
  end
end
