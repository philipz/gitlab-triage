# frozen_string_literal: true

require 'spec_helper'

describe 'summary policies' do
  include_context 'integration'

  let(:created_at) { Time.parse(issue[:created_at]) }

  let(:first_issue) do
    issue.merge(title: 'First issue')
  end

  let(:second_issue) do
    issue.merge(title: 'Second issue', created_at: (created_at + 10).iso8601)
  end

  let(:third_issue) do
    issue.merge(title: 'Third issue', created_at: (created_at + 20).iso8601)
  end

  let(:issues) { [first_issue, second_issue, third_issue] }

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end
  end

  it 'creates a grand summary' do
    rule = <<~YAML
      resource_rules:
        issues:
          summaries:
            - name: Grand summary
              actions:
                summarize:
                  title: Grand summary for {{type}}
                  summary: |
                    Grand summary for following {{type}}:

                    {{items}}

              rules:
                - name: First summary
                  limits:
                    most_recent: 1
                  actions:
                    summarize:
                      item: '- {{title}}'
                      summary: |
                        First summary for following {{type}}:

                        {{items}}

                - name: Second summary
                  limits:
                    oldest: 1
                  actions:
                    summarize:
                      item: '- {{title}}'
                      summary: |
                        Second summary for following {{type}}:

                        {{items}}
    YAML

    grand_title = 'Grand summary for issues'

    first_summary = <<~MARKDOWN.chomp
      First summary for following issues:

      - Third issue
    MARKDOWN

    second_summary = <<~MARKDOWN.chomp
      Second summary for following issues:

      - First issue
    MARKDOWN

    grand_summary = <<~MARKDOWN.chomp
      Grand summary for following issues:

      #{first_summary}

      #{second_summary}
    MARKDOWN

    stub_post_grand_summary = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: grand_title, description: grand_summary },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post_grand_summary)
  end

  context 'when no resources for the first summary' do
    it 'ignores the first summary' do
      rule = <<~YAML
        resource_rules:
          issues:
            summaries:
              - name: Grand summary
                actions:
                  summarize:
                    title: Grand summary for {{type}}
                    summary: |
                      Grand summary for following {{type}}:

                      {{items}}

                rules:
                  - name: First summary
                    conditions:
                      labels:
                        - apple
                    actions:
                      summarize:
                        item: '- {{title}}'
                        summary: |
                          First summary for following {{type}}:

                          {{items}}

                  - name: Second summary
                    limits:
                      oldest: 1
                    actions:
                      summarize:
                        item: '- {{title}}'
                        summary: |
                          Second summary for following {{type}}:

                          {{items}}
      YAML

      grand_title = 'Grand summary for issues'

      second_summary = <<~MARKDOWN.chomp
        Second summary for following issues:

        - First issue
      MARKDOWN

      grand_summary = <<~MARKDOWN.chomp
        Grand summary for following issues:

        #{second_summary}
      MARKDOWN

      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { labels: 'apple', per_page: 100 }
      ) { [] }

      stub_post_grand_summary = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        body: { title: grand_title, description: grand_summary },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_grand_summary)
    end
  end

  context 'when no resources for anything' do
    it 'does not post anything' do
      rule = <<~YAML
        resource_rules:
          issues:
            summaries:
              - name: Grand summary
                actions:
                  summarize:
                    title: Grand summary for {{type}}
                    summary: |
                      Grand summary for following {{type}}:

                      {{items}}

                rules:
                  - name: First summary
                    conditions:
                      labels:
                        - apple
                    actions:
                      summarize:
                        item: '- {{title}}'
                        summary: |
                          First summary for following {{type}}:

                          {{items}}

                  - name: Second summary
                    limits:
                      oldest: 1
                    conditions:
                      labels:
                        - orange
                    actions:
                      summarize:
                        item: '- {{title}}'
                        summary: |
                          Second summary for following {{type}}:

                          {{items}}
      YAML

      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { labels: 'apple', per_page: 100 }
      ) { [] }

      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { labels: 'orange', per_page: 100 }
      ) { [] }

      perform(rule)
    end
  end
end
