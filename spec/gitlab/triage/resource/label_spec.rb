require 'spec_helper'

require 'gitlab/triage/resource/label'

describe Gitlab::Triage::Resource::Label do
  include_context 'network'

  let(:resource) do
    {
      id: 1,
      project_id: 123,
      name: 'bug',
      description: 'a bug',
      color: '#d9534f',
      priority: 10,
      added_at: added_at.iso8601
    }
  end

  let(:added_at) { Time.new(2017, 1, 1) }

  subject { described_class.new(resource, network: network) }

  it_behaves_like 'resource fields'
end
