# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/context'

describe Gitlab::Triage::Resource::Context do
  include_context 'network'

  let(:resource) { {} }

  subject { described_class.build(resource, network: network) }

  describe '.build' do
    let(:resource) { { type: type } }

    context 'when resource type is issues' do
      let(:type) { 'issues' }

      it 'returns an instance of Resource::Issue' do
        is_expected.to be_a(Gitlab::Triage::Resource::Issue)
      end
    end

    context 'when resource type is merge_requests' do
      let(:type) { 'merge_requests' }

      it 'returns an instance of Resource::MergeRequest' do
        is_expected.to be_a(Gitlab::Triage::Resource::MergeRequest)
      end
    end

    context 'when there is not resource type' do
      let(:type) {}

      it 'returns an instance of Resource::Base' do
        is_expected.to be_a(Gitlab::Triage::Resource::Base)
      end
    end
  end

  describe '#eval' do
    it 'evaluate on the instance' do
      expect(subject).to receive(:instance_version)

      subject.eval('instance_version')
    end

    context 'when there is an internal error' do
      before do
        def subject.instance_version
          raise NameError
        end
      end

      it 'captures the backtrace pointing to where the actual error raised' do
        expect { subject.eval('instance_version') }
          .to raise_error(NameError) { |error|
            expect(error.backtrace.first).to include('instance_version')
          }
      end
    end
  end

  describe '#instance_version' do
    it 'gives an instance version object' do
      instance_version = subject.__send__(:instance_version)

      expect(instance_version)
        .to be_kind_of(Gitlab::Triage::Resource::InstanceVersion)
    end
  end
end
