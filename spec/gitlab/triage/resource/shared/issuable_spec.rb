# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/base'
require 'gitlab/triage/resource/shared/issuable'

describe Gitlab::Triage::Resource::Shared::Issuable do
  include_context 'network'

  let(:resource) { {} }
  let(:name) { 'Gitlab::Triage::Resource::TestModel' }

  let(:model) do
    Class.new(Gitlab::Triage::Resource::Base) do
      include(Gitlab::Triage::Resource::Shared::Issuable)
    end
  end

  before do
    allow(model).to receive(:name).and_return(name)
  end

  subject do
    model.new(resource, network: network)
  end

  describe '#milestone' do
    context 'when there is no milestone' do
      it 'gives something falsey' do
        expect(subject.__send__(:milestone)).to be_falsey
      end
    end

    context 'when there is a milestone' do
      let(:resource) { { milestone: { id: 1 } } }

      it 'gives the milestone based on the resource' do
        milestone = subject.__send__(:milestone)

        expect(milestone).to be_kind_of(Gitlab::Triage::Resource::Milestone)
        expect(milestone.id).to eq(1)
      end
    end
  end

  describe '#labels' do
    let(:resource) { { labels: %w[this is an apple] } }

    it 'gives an array of Label with the respective names' do
      labels = subject.__send__(:labels)

      expect(labels.map(&:name)).to eq(resource[:labels])
    end

    it 'sets the parent to the resource' do
      labels = subject.__send__(:labels)

      expect(labels.map(&:parent)).to eq([subject] * 4)
    end
  end

  context 'with label events' do
    # Issues can have labels which doesn't show in the events, because
    # events are only stored later.
    let(:resource) { { project_id: 452, iid: 321, labels: %w[old bug] } }

    let(:events_resource) do
      [
        {
          id: 1,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'add',
          label: { name: 'bug' }
        },
        {
          id: 2,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'add',
          label: { name: 'feature' }
        },
        {
          id: 3,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'remove',
          label: { name: 'bug' }
        },
        {
          id: 4,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'add',
          label: { name: 'bug' }
        },
        {
          id: 5,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'remove',
          label: { name: 'feature' }
        }
      ]
    end

    before do
      stub_api(
        :get,
        "#{base_url}/projects/452/test_models/321/resource_label_events",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => network.options.token }) do
        events_resource
      end
    end

    describe '#label_events' do
      it 'gives an array of LabelEvent with the respective attributes' do
        events = subject.__send__(:label_events)

        expect(events).not_to be_empty

        events.each.with_index do |event, index|
          %i[id resource_type resource_id action].each do |field|
            expect(event.public_send(field))
              .to eq(events_resource.dig(index, field))
          end

          expect(event).to be_a(Gitlab::Triage::Resource::LabelEvent)
          expect(event.parent).to eq(subject)
          expect(event.label.parent).to eq(event)
          expect(event.label.name)
            .to eq(events_resource.dig(index, :label, :name))
        end
      end
    end

    describe '#labels_with_details' do
      it 'returns labels which have details from the events' do
        labels = subject.__send__(:labels_with_details)

        # It only has bug because feature is not in labels and
        # old is not in the events.
        expect(labels.map(&:name)).to eq(%w[bug])
        expect(labels.map(&:class)).to eq([Gitlab::Triage::Resource::Label])
      end
    end

    describe '#labels_chronologically' do
      let(:events_resource) do
        [
          {
            id: 1,
            action: 'add',
            created_at: Time.new(2017, 1, 1),
            label: { name: 'bug' }
          },
          {
            id: 2,
            action: 'add',
            created_at: Time.new(2016, 1, 1),
            label: { name: 'old' }
          }
        ]
      end

      it 'returns the labels with detailed sorted chronologically' do
        labels = subject.__send__(:labels_chronologically)

        expect(labels.map(&:name)).to eq(%w[old bug])
      end
    end
  end
end
