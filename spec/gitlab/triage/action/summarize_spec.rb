require 'spec_helper'

require 'gitlab/triage/action/summarize'
require 'gitlab/triage/policies/rule_policy'

require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action::Summarize do
  include_context 'network'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ]
  end

  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      'issues',
      { name: 'Test summarize rule', actions: actions_hash },
      resources,
      network
    )
  end

  let(:actions_hash) do
    {
      summarize: {
        title: title,
        item: item,
        summary: summary
      }
    }
  end
  let(:title) { 'Issue title' }
  let(:item) { '- [ ] [{{title}}]({{web_url}})' }
  let(:summary) { "Here's the summary:\n\n{{items}}" }

  let(:description) do
    <<~TEXT.chomp
      Here's the summary:

      - [ ] [Issue #0](http://example.com/0)
      - [ ] [Issue #1](http://example.com/1)
    TEXT
  end

  subject do
    described_class.new(
      policy: policy, network: network)
  end

  describe '#act' do
    it 'posts the right issue' do
      stub_post = stub_api(
        :post,
        "http://test.com/api/v4/projects/123/issues",
        body: { title: title, description: description }
      )

      subject.act

      assert_requested(stub_post)
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end

    context 'when issue title is blank' do
      let(:title) { '' }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end
  end
end
